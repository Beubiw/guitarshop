CREATE DATABASE guitarshop;

/*CREATE TABLE */

CREATE TABLE Guitar
(
id INT Identity (1,1) NOT NULL,
name VARCHAR NOT NULL,
stringNbr INT NOT NULL,
price FLOAT NOT NULL,
quantity INT NOT NULL,
color VARCHAR NOT NULL,
description VARCHAR NOT NULL,
categoryId INT NOT NULL,
brandId INT NOT NULL,
);

CREATE TABLE GuitarCategory
(
id INT Identity (1,1) NOT NULL,
name VARCHAR(50) NOT NULL,

);

CREATE TABLE Brand
(
id int Identity (1,1) NOT NULL,
name VARCHAR(50) NOT NULL,
);

CREATE TABLE Account
(
id INT Identity (1,1) NOT NULL,
email VARCHAR(50) NOT NULL,
firstname VARCHAR(50) NOT NULL,
lastname VARCHAR(50) NOT NULL,
phone VARCHAR(50) NOT NULL,
password VARCHAR(255) NOT NULL,
);

CREATE TABLE GuitarImage
(
id INT Identity (1,1) NOT NULL,
name VARCHAR(50) NOT NULL,
guitarId INT NOT NULL,
);

/*CONSTRAINT*/
ALTER TABLE GuitarCategory
ADD CONSTRAINT pk_guitar_category PRIMARY KEY (id);

ALTER TABLE Brand
ADD CONSTRAINT pk_brand PRIMARY KEY (id);

ALTER TABLE Guitar
ADD CONSTRAINT pk_guitar PRIMARY KEY (id);

ALTER TABLE Guitar
ADD CONSTRAINT fk_guitar_category FOREIGN KEY (categoryId) REFERENCES GuitarCategory(id),
CONSTRAINT fk_guitar_brand FOREIGN KEY (brandId) REFERENCES Brand(id);

ALTER TABLE GuitarImage
ADD CONSTRAINT fk_image_guitar FOREIGN KEY (guitarId) REFERENCES Guitar(id);

/*INSERT*/

INSERT INTO GuitarCategory(name)
VALUES
('Acoustic'),
('Electro-acoustic'),
('Semi-acoustic'),
('Electric'),
('Bass'),
('Mandolin'),
('Banjo'),
('Ukulele'),
('Classical');

INSERT INTO Brand(name)
VALUES
('Gibson'),
('Fender'),
('Godin'),
('Ibanez'),
('Epiphone');

INSERT INTO Guitar(name, stringNbr, price, quantity, color, description, categoryId, brandId)
VALUES
('American Professional II Stratocaster', '6', '2176.79', '3', 'Sunburst',
'Aliquam porttitor sapien eget metus aliquam, id molestie sem euismod. Etiam dapibus vel ante laoreet hendrerit. Vivamus non libero eget tellus tincidunt posuere posuere non dolor. Proin congue pellentesque nisl. Maecenas nec ligula sit amet nunc rhoncus accumsan. Vestibulum non ipsum id odio semper pulvinar. Aenean a placerat tellus. Pellentesque eleifend vestibulum bibendum. Quisque scelerisque quam magna, eu porta velit tristique venenatis. Pellentesque consectetur rutrum ipsum, eget dictum risus luctus a. Ut efficitur diam non facilisis cursus.',
'22', '12'),
('Noventa Stratocaster', '6', '1399.99', '2', 'Crimson Red Transparent',
'Suspendisse consequat lacinia diam, ut consectetur ligula consectetur vitae. Cras eu semper mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc sollicitudin dolor id ligula sollicitudin, non efficitur leo facilisis. Suspendisse ut maximus justo.',
'22', '12'),
('Les Paul', '6', '1499.00', '7', 'Satin Cherry',
'Duis eu risus erat. Integer dapibus commodo lacus, quis maximus nisi feugiat ornare. Ut luctus suscipit elit, sit amet mollis sem bibendum quis. Mauris iaculis justo dapibus erat pulvinar, id aliquam nulla faucibus. Nulla imperdiet suscipit nibh convallis feugiat. Quisque lacus dolor, accumsan quis viverra vitae, semper ut dolor. Cras molestie at purus sed interdum. Donec dapibus quam id dui lacinia vestibulum. Nunc nisi enim, vestibulum sed dolor sed, rhoncus efficitur nibh. Nam eu purus quis magna vestibulum egestas vel eget urna. Vestibulum vehicula, nulla quis cursus scelerisque, orci urna fringilla elit, non varius nisi sem id augue. Proin luctus, ex et vulputate consectetur, justo elit dapibus eros, vitae pellentesque elit ligula at leo.',
'22', '11');


/*MODIFICATION*/
ALTER TABLE Guitar
ALTER COLUMN description TEXT;

ALTER TABLE Guitar
ALTER COLUMN name VARCHAR(255);

ALTER TABLE Guitar
ALTER COLUMN color VARCHAR(255);

/*SELECT*/
SELECT * FROM GuitarCategory
SELECT * FROM Brand
SELECT * FROM Guitar
