import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeNavbar from "./component/Navbar";
import Home from "./Home/Home";
import Login from "./login/Login";
import CategGuitar from "./Category/CategGuitar";

function App() {
  return (
    <>
      <BrowserRouter>
        <HomeNavbar></HomeNavbar>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/category/:id" element={<CategGuitar />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
