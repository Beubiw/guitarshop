import { Container, Nav, NavDropdown, Navbar } from "react-bootstrap";
import { useState, useEffect } from "react";

function HomeNavbar() {
  const [categList, setCategList] = useState([]);

  useEffect(() => {
    //Get guitar categories list
    async function getGuitarCateg() {
      const srvURL = `http://localhost:5000/guitarCategory`;
      const response = await fetch(srvURL, {
        method: "GET",
      });
      if (response.ok) {
        const data = await response.json();
        setCategList(data.category);
      }
    }

    return () => {
      getGuitarCateg();
    };
  }, []);

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="/">Guitar shop</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/link">Link</Nav.Link>
              <NavDropdown title="Categories" id="basic-nav-dropdown">
                {categList &&
                  categList.map((categ) => (
                    <NavDropdown.Item
                      key={categ.id}
                      href={"/category/" + categ.id}
                    >
                      {categ.name}
                    </NavDropdown.Item>
                  ))}
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Separated link
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
          <Nav className="justify-content-end">
            <Nav.Link href="/login">Login</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export default HomeNavbar;
