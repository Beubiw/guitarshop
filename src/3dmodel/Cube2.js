/* eslint-disable no-unused-expressions */
import { useRef, Suspense, useState } from "react";
import { Canvas } from "@react-three/fiber";
import { useGLTF } from "@react-three/drei";
import { proxy, useSnapshot } from "valtio";
import { HexColorPicker } from "react-colorful";

const state = proxy({
  current: null,
  items: {
    cube: "#ff0000",
  },
});

function Cube({ ...props }) {
  const group = useRef();
  const snap = useSnapshot(state);
  const { nodes, materials } = useGLTF("cube2.glb");
  const [hovered, set] = useState(null);
  return (
    <group
      ref={group}
      {...props}
      dispose={null}
      onPointerOver={(e) => {
        e.stopPropagation(), set(e.object.material.name);
      }}
      onPointerOut={(e) => {
        e.intersections.length === 0 && set(null);
      }}
      onPointerDown={(e) => {
        e.stopPropagation();
        state.current = e.object.material.name;
      }}
      onPointerMissed={(e) => {
        state.current = null;
      }}
    >
      <mesh
        material-color={snap.items.cube}
        geometry={nodes.Cube.geometry}
        material={materials.Material}
      />
    </group>
  );
}

function Picker() {
  const snap = useSnapshot(state);
  return (
    <div style={{ display: snap.current ? "block" : "none" }}>
      <HexColorPicker
        className="picker"
        color={snap.items[snap.current]}
        onChange={(color) => (state.items[snap.current] = color)}
      />
      <h1>{snap.current}</h1>
    </div>
  );
}

export default function GuitarModel() {
  return (
    <>
      <Picker />
      <Canvas>
        <ambientLight intensity={0.5} />
        <Suspense fallback={null}>
          <Cube></Cube>
        </Suspense>
      </Canvas>
    </>
  );
}
