function Login() {
  return (
    <>
      <div className="columns">
        <div className="column"></div>
        <div className="column is-3">
          <form className="box">
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input
                  className="input"
                  type="email"
                  placeholder="e.g. alex@example.com"
                />
              </div>
            </div>

            <div className="field">
              <label className="label">Password</label>
              <div className="control">
                <input
                  className="input"
                  type="password"
                  placeholder="********"
                />
              </div>
            </div>

            <button className="button is-primary">Sign in</button>
          </form>
        </div>
        <div className="column"></div>
      </div>
    </>
  );
}

export default Login;
