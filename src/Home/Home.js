import "./Home.css";
import ModelViewer from "../component/ModelViewer";
import GuitarModel from "../3dmodel/Cube2";

function Home() {
  return (
    <>
      <div className="guitarModelBar">
        <ModelViewer scale="40" modelPath={"guitarTest.glb"} />
      </div>
      <div className="guitarModelBar">
        <GuitarModel />
      </div>
    </>
  );
}

export default Home;
