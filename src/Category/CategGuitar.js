import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function CategGuitar() {
  const { id } = useParams();
  const [category, setCategory] = useState();

  useEffect(() => {
    //Get category
    async function getGuitarCateg() {
      console.log(id);
      const categId = id;
      console.log(categId);
      const srvURL = `http://localhost:5000/guitarCategory/${categId}`;
      const response = await fetch(srvURL, {
        method: "GET",
      });
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setCategory(data.category);
      }
    }

    return () => {
      getGuitarCateg();
    };
  }, [id]);

  return (
    <>
      {category && (
        <div>
          <h1>{category.name}</h1>
          <img
            src="https://bulma.io/images/placeholders/128x128.png"
            className="img-fluid"
            alt=""
          />
          <h3>Description</h3>
        </div>
      )}
    </>
  );
}

export default CategGuitar;
